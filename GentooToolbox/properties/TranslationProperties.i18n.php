<?php
/**
 * GentooToolbox
 *
 * @ingroup Extensions
 * @author Alex Legler <a3li@gentoo.org>
 * @version 1.0
 * @license http://www.gnu.org/copyleft/gpl.html GNU General Public License 2.0 or later
 */

$messages['en'] = array(
	'gtbx-prop-lang' => 'Language',
	'gtbx-prop-trans' => 'Is Translation Page',
);
