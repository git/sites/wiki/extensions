<?php
/**
 * GentooToolbox
 *
 * @ingroup Extensions
 * @author Alex Legler <a3li@gentoo.org>
 * @version 1.0
 * @license http://www.gnu.org/copyleft/gpl.html GNU General Public License 2.0 or later
 */

use SMW\DIProperty;
use SMW\PropertyRegistry;

class GTBXTranslationProperties {

	public static function setupProperties() {
		PropertyRegistry::getInstance()->registerProperty('___LANG', '_str', wfMessage('gtbx-prop-lang')->inContentLanguage()->text());
		PropertyRegistry::getInstance()->registerPropertyAlias('___LANG', 'Language');

		PropertyRegistry::getInstance()->registerProperty('___TRANS', '_boo', wfMessage('gtbx-prop-trans')->inContentLanguage()->text());
		PropertyRegistry::getInstance()->registerPropertyAlias('___TRANS', 'Is Translation Page');

		return true;
	}

	public static function updateDataBefore($store, $data) {
		$subject = $data->getSubject();
		$title = Title::makeTitle($subject->getNamespace(), $subject->getDBKey());
		$wikipage = WikiPage::factory($title);

		if (is_null($title) || is_null($wikipage)) {
			return true;
		}

		// Property 1: Is translation page
		$property = new DIProperty('___TRANS');
		$is_translation_page = !(TranslatablePage::isTranslationPage($title) === false);
		$data_item = new SMWDIBoolean($is_translation_page);

		$data->addPropertyObjectValue($property, $data_item);

		// Property 2: Language?
		$property = new DIProperty('___LANG');
		global $wgLanguageCode;
		$language_code = $wgLanguageCode;

		if ($is_translation_page) {
			list( , $code ) = TranslateUtils::figureMessage( $title->getText() );
			$language_code = $code;
		}

		$data_item = new SMWDIString($language_code);

		$data->addPropertyObjectValue($property, $data_item);

	}
}
