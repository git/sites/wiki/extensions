<?php

class GTBXLocalizedMsg { // implements MediaWiki\Hook\ParserFirstCallInitHook {
	public static function onParserFirstCallInit ( $parser ) {
		$parser->setFunctionHook( 'msg', 'GTBXLocalizedMsg::LocalizedMsgParserFunction' );
		return true;
	}

	public static function LocalizedMsgParserFunction( $parser, $message, $language = 'en' ) {
		$title = $parser->getTitle(); 
		$is_translation_page = !( TranslatablePage::isTranslationPage( $title ) === false );
		global $wgLanguageCode;
		$language_code = $wgLanguageCode;

		if ( $is_translation_page ) {
			list( , $code ) = TranslateUtils::figureMessage( $title->getText() );
			$language_code = $code;
		}

		return wfMessage( $message )->inLanguage( $language_code )->escaped();
	}
}
