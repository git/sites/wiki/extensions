<?php
/**
 * GentooToolbox
 *
 * @ingroup Extensions
 * @author Alex Legler <a3li@gentoo.org>
 * @version 1.0
 * @license http://www.gnu.org/copyleft/gpl.html GNU General Public License 2.0 or later
 */

if (!defined('MEDIAWIKI')) {
	echo "This is an extension to the MediaWiki package and cannot be run standalone.\n";
	die(-1);
}

if (!defined('SMW_VERSION')) {
	echo "Semantic MediaWiki not found.\n";
	die(-1);
}

define('GTBX_VERSION', '0.0.1');

$wgExtensionCredits['semantic'][] = array(
	'path'           => __FILE__,
	'name'           => 'Gentoo Toolbox: Translation properties',
	'version'        => GTBX_VERSION,
	'author'         => 'Alex Legler', 
	'url'            => 'https://wiki.gentoo.org/',
);

$wgExtensionCredits['other'][] = array(
	'path'           => __FILE__,
	'name'           => 'Gentoo Toolbox',
	'version'        => GTBX_VERSION,
	'author'         => 'Alex Legler', 
	'url'            => 'https://wiki.gentoo.org/',
);

$dir = dirname(__FILE__) . '/';

// Translation Properties
$wgExtensionMessagesFiles['GTBX_TranslationProperties'] = $dir . '/properties/TranslationProperties.i18n.php';
$wgAutoloadClasses['GTBXTranslationProperties']			= $dir . '/properties/TranslationProperties.body.php';

$wgHooks['smwInitProperties'][] = 'GTBXTranslationProperties::setupProperties';
$wgHooks['SMWStore::updateDataBefore'][] = 'GTBXTranslationProperties::updateDataBefore';

// LocalizedMessage
$wgAutoloadClasses['GTBXLocalizedMsg'] = $dir . '/localizedmsg/LocalizedMsg.php';
$wgHooks['ParserFirstCallInit'][] = 'GTBXLocalizedMsg::SetUpLocalizedMsgParserFunction';
