<?php

use MediaWiki\MediaWikiServices;

class GentooPackages { // implements MediaWiki\Hook\ParserFirstCallInitHook {
  public static function packageInfo($input, array $args, Parser $parser, PPFrame $frame) {
    $parser->getOutput()->addModules('ext.gentooPackages');
    $atom = $args['atom'];
    $type = $args['type'];

    if ($atom === NULL) {
      return "Package name missing";
    }
    try {
       $objCache = MediaWikiServices::getInstance()->getMainWANObjectCache();
       $cacheKeyString = $objCache->makeKey('packageInfo', $atom, $type);
       $packageInfo = $objCache->getWithSetCallback($cacheKeyString, $objCache::TTL_DAY,
           function( $oldValue, &$ttl, array &$setOpts ) use ( $atom, $type ) {
		return self::fetchOrError($atom, $type);
           }
       );
       return [$packageInfo, 'markerType' => 'nowiki'];
    } catch (Exception $ex) {
       return [$ex->getMessage(), 'markerType' => 'nowiki'];
    }
  }

  public static function fetchOrError($atom, $type) {
    global $wgVersion;
    $url = "https://packages.gentoo.org/packages/${atom}.json";
    if ($type !== 'use') {
        throw new UnexpectedValueException('<div class="alert alert-danger">Unknown type parameter value.</div>');
    }

    if(version_compare( $wgVersion, '1.33', '<=' ))
      $json_str = Http::get($url);
    else {
      $httpRequest = MediaWikiServices::getInstance()->getHttpRequestFactory()
		->create($url, [ 'method' => 'GET' ], __METHOD__ );
      $status = $httpRequest->execute();
      if ($status->isOK())
        $json_str = $httpRequest->getContent();
      else
        $json_str = false;
    }

    if ($json_str === false) {
      throw new ErrorException('<div class="alert alert-danger">Cannot load package information. Is the atom <em>' . htmlspecialchars($atom) . '</em> correct?</div>');
    } else {
      $json = json_decode($json_str, true);
      return self::render($json);
    }
  }

  private static function render($json) {
    $use_flags = self::renderFlags($json);
    $updated_at = strftime('%Y-%m-%d %H:%M', strtotime($json['updated_at']));
    $desc = htmlspecialchars($json['description']);

    return <<<HTML
      <div class="panel panel-default gp-panel">
        <div class="panel-heading gp-panel-heading">
          <h3 class="panel-title">
            <span class="text-muted">USE flags for</span>
            <a href="${json['href']}">${json['atom']}</a>
            <small><span class="fa fa-external-link-square"></span></small>
            <small class="gp-pkg-desc">${desc}</small>
          </h3>
        </div>
        <div class="table-responsive gp-useflag-table-container">
          ${use_flags}
        </div>
        <div class="panel-footer gp-panel-footer">
          <small class="pull-right">
            Data provided by the <a href="https://packages.gentoo.org">Gentoo Package Database</a>
            &middot;
            Last update: ${updated_at}
          </small>
          <small>
            <a href="/wiki/Handbook:AMD64/Working/USE">More information about USE flags</a>
          </small>
        </div>
      </div>
HTML;
  }

  private static function renderFlags($json) {
    $flags = self::sortFlags($json);

    $html = <<<HTML
      <table class="table gp-useflag-table">
HTML;

    foreach ($flags as $flag) {
      $name = htmlspecialchars($flag['name']);
      $desc = htmlspecialchars($flag['description']);

      $html .= <<<HTML
        <tr>
          <td>
            <code><a href="https://packages.gentoo.org/useflags/${name}">${name}</a></code>
          </td>
          <td>
            ${desc}
          </td>
        </tr>
HTML;
    }

    $html .= <<< HTML
      </table>
HTML;

    return $html;
  }

  private static function sortFlags($json) {
    $merged_flags = [];
    foreach(array_merge($json['use']['global'], $json['use']['local']) as $flag)
      $merged_flags[$flag['name']] = $flag;
    ksort($merged_flags);
    return $merged_flags;
  }

  public static function onParserFirstCallInit($parser) {
    $parser->setHook('package-info', 'GentooPackages::packageInfo');
  }
}
