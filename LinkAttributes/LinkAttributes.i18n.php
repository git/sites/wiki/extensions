<?php
/**
 * Internationalisation file for extension Link Attributes
 *
 * @file LinkAttributes.i18n.php
 * @ingroup Extensions
 */

$messages = [
	'en' => [
		'linkattr-desc' => 'Lets you add or remove HTML attributes in links.',
	]
];


/** Swedish (Svenska)
 * @author Rotsee
 */
$messages['sv'] = array(
        'linkattr-desc' => 'Gör det möjligt att sätta vissa html-attribut på länkar.',
);
$messages['pt'] = array(
        'linkattr-desc' => 'Permite adicionar ou remover atributios HTML em links.',
);
