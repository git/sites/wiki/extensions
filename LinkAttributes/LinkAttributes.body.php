<?php

use MediaWiki\Linker\LinkRenderer;
use MediaWiki\Linker\LinkTarget;

class LinkAttributes {
	private static $attrsAllowed=[ 'rel', 'rev', 'charset ', 'type', 'hreflang', 'itemprop', 'media', 'title', 'accesskey', 'target', 'class' ];

	private static function doExtractAttributes ( &$text, &$attribs ) {

		global $wgRequest;
		if ( $wgRequest->getText( 'action' ) == 'edit' ) {
			return false;
		}

		/* No user input */
		if ( null === $text )
			return false;

		/* Extract attributes, separated by | or ¦. /u is for unicode, to recognize the ¦.*/
		$arr = preg_split( '/[|¦]/u', $text );
		$text = array_shift( $arr );

		foreach ( $arr as $a ) {

			$pair = explode( '=', $a );

			/* Only go ahead if we have a aaa=bbb pattern, and aaa i an allowed attribute */
			if ( isset( $pair[1] ) && in_array( $pair[0], static::$attrsAllowed ) ) {

				/* Add to existing attribute, or create a new */
				if ( isset( $attribs[$pair[0]] ) ) {
					$attribs[$pair[0]] = $attribs[$pair[0]] . ' ' . $pair[1];
				} else {
					$attribs[$pair[0]] = $pair[1];
				}
			}

		}

		return true;

	}

	public static function ExternalLink ( &$url, &$text, &$link, &$attribs ) {

		self::doExtractAttributes ( $text, $attribs );
		return true;
	}

	public static function InternalLink ( $skin, $target, &$text, &$customAttribs, &$query, &$options, &$ret ) {

		self::doExtractAttributes ( $text, $customAttribs );
		return true;

	}

	public static function onHtmlPageLinkRendererBegin(
		LinkRenderer $linkRenderer, LinkTarget $target, &$text, &$extraAttribs, &$query, &$ret ) {

		self::doExtractAttributes ( $text, $extraAttribs );
		return true;

	}

}
